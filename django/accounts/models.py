# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        managed = False

    objects = UserManager()
    USERNAME_FIELD = 'email'

    email = models.EmailField(unique=True)
    first_name = models.CharField(_('First name'), max_length=300)
    last_name = models.CharField(_('Last name'), max_length=300)

    is_staff = models.BooleanField(_('staff status'),
                                   default=False,
                                   help_text=_('admin ?'))
    is_active = models.BooleanField(_('active'),
                                    default=True,
                                    help_text=_('Utilisateur actif ?'))

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name.upper()
