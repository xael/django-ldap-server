all: build publish

build:
	docker build -t django-ldap-server:latest .

publish:
	docker tag django-ldap-server:latest xael/django-ldap-server:latest
	docker push xael/django-ldap-server:latest

private_publish:
	docker tag django-ldap-server:latest registry.hespul.org:5000/django-ldap-server:latest
	docker push registry.hespul.org:5000/django-ldap-server:latest

.PHONY: build publish private_publish
